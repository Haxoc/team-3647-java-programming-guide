\contentsline {section}{\numberline {1}Prerequisites for Robotics Programming}{5}{section.1}% 
\contentsline {subsection}{\numberline {1.1}General Programming Requirements}{5}{subsection.1.1}% 
\contentsline {subsection}{\numberline {1.2}Robotics Programming Requirements}{5}{subsection.1.2}% 
\contentsline {section}{\numberline {2}Writing actual code for the robot}{7}{section.2}% 
\contentsline {subsection}{\numberline {2.1}The Style Guide}{7}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}More Specific to Our Code}{7}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Running the Robot}{8}{subsection.2.3}% 
\contentsline {subsubsection}{\numberline {2.3.1}Driverstation}{8}{subsubsection.2.3.1}% 
\contentsline {subsubsection}{\numberline {2.3.2}Enabling the Robot for the first time}{8}{subsubsection.2.3.2}% 
\contentsline {subsubsection}{\numberline {2.3.3}Emergency Stop (E-Stop)}{8}{subsubsection.2.3.3}% 
\contentsline {subsection}{\numberline {2.4}Getting Joystick Values}{8}{subsection.2.4}% 
\contentsline {subsection}{\numberline {2.5}The Constants.java class}{9}{subsection.2.5}% 
\contentsline {subsection}{\numberline {2.6}Interfacing with CAN devices}{9}{subsection.2.6}% 
\contentsline {subsection}{\numberline {2.7}Comments regarding robot code style}{10}{subsection.2.7}% 
\contentsline {subsubsection}{\numberline {2.7.1}The Robot is Divided into Subsystems}{10}{subsubsection.2.7.1}% 
\contentsline {subsubsection}{\numberline {2.7.2}Wrapper classes}{10}{subsubsection.2.7.2}% 
\contentsline {subsubsection}{\numberline {2.7.3}Initializing Motors}{10}{subsubsection.2.7.3}% 
\contentsline {subsubsection}{\numberline {2.7.4}Smart motors}{11}{subsubsection.2.7.4}% 
\contentsline {subsubsection}{\numberline {2.7.5}PeriodicIO}{11}{subsubsection.2.7.5}% 
\contentsline {subsubsection}{\numberline {2.7.6}Autonomous}{11}{subsubsection.2.7.6}% 
\contentsline {subsection}{\numberline {2.8}Common mechanical and electrical related programming issues}{11}{subsection.2.8}% 
\contentsline {subsection}{\numberline {2.9}Open-loop vs. Closed-loop control}{11}{subsection.2.9}% 
\contentsline {subsubsection}{\numberline {2.9.1}Voltage Compensation}{12}{subsubsection.2.9.1}% 
\contentsline {section}{\numberline {3}Advanced Functions}{13}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Enums}{13}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}Notifier Thread}{13}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Lambdas}{14}{subsection.3.3}% 
\contentsline {subsection}{\numberline {3.4}Inheritance}{15}{subsection.3.4}% 
\contentsline {section}{\numberline {4}Motor Controllers}{16}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Smart Motor Controllers}{16}{subsection.4.1}% 
\contentsline {subsubsection}{\numberline {4.1.1}TalonSRX}{16}{subsubsection.4.1.1}% 
\contentsline {subsubsection}{\numberline {4.1.2}SPARK MAX}{16}{subsubsection.4.1.2}% 
\contentsline {subsection}{\numberline {4.2}"Dumb" motor controllers}{17}{subsection.4.2}% 
\contentsline {subsubsection}{\numberline {4.2.1}VictorSPX}{17}{subsubsection.4.2.1}% 
\contentsline {subsubsection}{\numberline {4.2.2}Following a TalonSRX}{17}{subsubsection.4.2.2}% 
\contentsline {subsection}{\numberline {4.3}Motors}{17}{subsection.4.3}% 
\contentsline {section}{\numberline {5}Sensors}{19}{section.5}% 
\contentsline {subsection}{\numberline {5.1}CTRE Mag Encoder}{19}{subsection.5.1}% 
\contentsline {subsection}{\numberline {5.2}Digital Input}{19}{subsection.5.2}% 
\contentsline {subsection}{\numberline {5.3}The CANifier}{19}{subsection.5.3}% 
\contentsline {subsection}{\numberline {5.4}CAN Digital Input}{20}{subsection.5.4}% 
\contentsline {section}{\numberline {6}Vision (Limelight)}{21}{section.6}% 
\contentsline {subsection}{\numberline {6.1}What is it?}{21}{subsection.6.1}% 
\contentsline {subsection}{\numberline {6.2}Documentation}{21}{subsection.6.2}% 
\contentsline {subsection}{\numberline {6.3}Best practices}{21}{subsection.6.3}% 
\contentsline {section}{\numberline {7}Using git (Github)}{22}{section.7}% 
\contentsline {subsection}{\numberline {7.1}Official git videos}{22}{subsection.7.1}% 
\contentsline {subsection}{\numberline {7.2}Committing Code}{22}{subsection.7.2}% 
\contentsline {subsection}{\numberline {7.3}Pull Request}{22}{subsection.7.3}% 
\contentsline {subsection}{\numberline {7.4}Official git tutorial}{22}{subsection.7.4}% 
\contentsline {section}{\numberline {8}Putting It All Together}{23}{section.8}% 
\contentsline {subsection}{\numberline {8.1}WPILIB Docs Links}{23}{subsection.8.1}% 
\contentsline {subsubsection}{\numberline {8.1.1}Installing VS Code}{23}{subsubsection.8.1.1}% 
\contentsline {subsubsection}{\numberline {8.1.2}Overall Docs}{23}{subsubsection.8.1.2}% 
\contentsline {subsubsection}{\numberline {8.1.3}Creating a Robot Program}{23}{subsubsection.8.1.3}% 
\contentsline {subsubsection}{\numberline {8.1.4}Installing Third Party Libraries}{23}{subsubsection.8.1.4}% 
\contentsline {subsubsection}{\numberline {8.1.5}Basic WPILIB Functions}{23}{subsubsection.8.1.5}% 
\contentsline {section}{\numberline {9}Projects}{24}{section.9}% 
\contentsline {subsection}{\numberline {9.1}Projects for all robots}{24}{subsection.9.1}% 
\contentsline {subsubsection}{\numberline {9.1.1}\IeC {\textquotedblleft }Just make it go\IeC {\textquotedblright }}{24}{subsubsection.9.1.1}% 
\contentsline {subsubsection}{\numberline {9.1.2}\IeC {\textquotedblleft }Let\IeC {\textquoteright }s slow it down\IeC {\textquotedblright }}{24}{subsubsection.9.1.2}% 
\contentsline {subsection}{\numberline {9.2}2019 Robot only}{25}{subsection.9.2}% 
\contentsline {subsubsection}{\numberline {9.2.1}\IeC {\textquotedblleft }Controlling Solenoids\IeC {\textquotedblright }}{25}{subsubsection.9.2.1}% 
\contentsline {subsubsection}{\numberline {9.2.2}\IeC {\textquotedblleft }Detecting cargo\IeC {\textquotedblright }}{25}{subsubsection.9.2.2}% 
\contentsline {subsubsection}{\numberline {9.2.3}``Let's Elevate"}{25}{subsubsection.9.2.3}% 
\contentsline {subsection}{\numberline {9.3}2020 Robot Only}{26}{subsection.9.3}% 
\contentsline {subsubsection}{\numberline {9.3.1}``Move that Hood\IeC {\textquotedblright }}{26}{subsubsection.9.3.1}% 
\contentsline {subsubsection}{\numberline {9.3.2}``Infinite Recharge Feeder\IeC {\textquotedblright }}{26}{subsubsection.9.3.2}% 
\contentsline {subsubsection}{\numberline {9.3.3}\IeC {\textquotedblleft }And Now the Intake\IeC {\textquotedblright }}{26}{subsubsection.9.3.3}% 
\contentsline {subsubsection}{\numberline {9.3.4}``What's My Character?"}{27}{subsubsection.9.3.4}% 
\contentsline {subsubsection}{\numberline {9.3.5}``Closing the Loop"}{27}{subsubsection.9.3.5}% 
\contentsline {subsubsection}{\numberline {9.3.6}``Dynamic Shooting"}{28}{subsubsection.9.3.6}% 
\contentsline {subsubsection}{\numberline {9.3.7}``How About some Rotation"}{28}{subsubsection.9.3.7}% 
\contentsline {subsubsection}{\numberline {9.3.8}``Can you see?"}{28}{subsubsection.9.3.8}% 
\contentsline {subsubsection}{\numberline {9.3.9}``Autonomous: here we go! - Part I"}{29}{subsubsection.9.3.9}% 
\contentsline {subsubsection}{\numberline {9.3.10}``Autonomous: here we go! - Part II"}{29}{subsubsection.9.3.10}% 
\contentsline {section}{\numberline {10}General Guidelines and Competition Rules}{30}{section.10}% 
\contentsline {section}{\numberline {A}PID Control}{31}{appendix.A}% 
\contentsline {subsection}{\numberline {A.1}The Basics}{31}{subsection.A.1}% 
\contentsline {subsection}{\numberline {A.2}Proportional}{31}{subsection.A.2}% 
\contentsline {subsection}{\numberline {A.3}Integral}{32}{subsection.A.3}% 
\contentsline {subsection}{\numberline {A.4}Derivative}{32}{subsection.A.4}% 
\contentsline {subsection}{\numberline {A.5}Feed-Forward}{33}{subsection.A.5}% 
\contentsline {subsubsection}{\numberline {A.5.1}Flywheel}{33}{subsubsection.A.5.1}% 
\contentsline {subsubsection}{\numberline {A.5.2}Arm affected by Gravity}{33}{subsubsection.A.5.2}% 
\contentsline {subsubsection}{\numberline {A.5.3}Cascade Elevator}{33}{subsubsection.A.5.3}% 
\contentsline {subsubsection}{\numberline {A.5.4}Drivetrain}{33}{subsubsection.A.5.4}% 
\contentsline {subsection}{\numberline {A.6}Using PID on Your Robot}{33}{subsection.A.6}% 
\contentsline {subsection}{\numberline {A.7}Tuning Methods}{34}{subsection.A.7}% 
\contentsline {subsection}{\numberline {A.8}Which ones to use}{35}{subsection.A.8}% 
\contentsline {section}{\numberline {B}Other Links}{36}{appendix.B}% 
\contentsline {subsection}{\numberline {B.1}Are Java Enums Considered Primitive or Reference Types?}{36}{subsection.B.1}% 
\contentsline {subsection}{\numberline {B.2}Finding Feed Forward of an arm}{36}{subsection.B.2}% 
\contentsline {section}{\numberline {C}Robot Code Referenced}{38}{appendix.C}% 
\contentsline {subsection}{\numberline {C.1}Joysticks.java}{38}{subsection.C.1}% 
\contentsline {subsection}{\numberline {C.2}Constants.java}{41}{subsection.C.2}% 
\contentsline {subsection}{\numberline {C.3}Arm.java}{52}{subsection.C.3}% 
\contentsline {subsection}{\numberline {C.4}SRXSubsystem.java}{56}{subsection.C.4}% 
\contentsline {subsection}{\numberline {C.5}ClosedLoopFactory.java}{63}{subsection.C.5}% 
\contentsline {subsection}{\numberline {C.6}Elevator.java}{68}{subsection.C.6}% 
\contentsline {subsection}{\numberline {C.7}BallShooter.java}{72}{subsection.C.7}% 
\contentsline {subsection}{\numberline {C.8}VisionController.java}{75}{subsection.C.8}% 
\contentsline {subsection}{\numberline {C.9}HatchGrabber.java}{79}{subsection.C.9}% 
